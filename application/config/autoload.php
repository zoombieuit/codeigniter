<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('mongo_db','session','parser', 'encrypt');
$autoload['helper'] = array('url_helper');
/*
Cú pháp load url không phải url_helper
*/
$autoload['helper'] = array('url');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();
