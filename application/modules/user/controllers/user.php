<?php
class User extends MY_Controller{
	function __construct(){
		parent::__construct();

		/*
		$this->load->spark('restclient/2.1.0');
		1) Loại bỏ $this->load->spark('restclient/2.1.0');  chúng tôi không yêu cầu phải dùng đến thư viện ngoài và case này không sử dụng các thư viện này,
		2) Chưa load model trang
		*/

		$this->load->library('rest');
		$config =  array('server' => base_url(''));
		$this->rest->initialize($config);
	}

	public function index(){
		$this->parser->parse('user_view',[]);
	}
}
?>
