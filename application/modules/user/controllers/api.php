<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller {
	function __construct(){
		parent::__construct();

		$this->load->model('user_model');
		$this->r = array('status'=>200,'message'=>'error');
	}

	public function index_post(){
		$draw = $_POST['draw'];
		$params = [
			'limit' => $_POST['length'],
			'skip'  => $_POST['start']
		];
		$users = $this->user_model->getList($params);

		$userResponse = [];
		foreach ($users as $user) {
			$userResponse[] = [
				'_id' => (string)$user['_id'],
				'name' => $user['name'],
				'birth_day' => $user['birth_day'],
				'sex' => $user['sex'],
				'balance' => $user['balance'],
				'created_date' => date('Y-m-d', $user['created_date']),
			];
		}

		$this->r['message'] = 'success';
		$this->r['data'] = $userResponse;

		// pagination
		$this->r['draw'] = $draw;
		$this->r['iTotalRecords'] = $this->user_model->countAll();
		$this->r['iTotalDisplayRecords'] = $this->user_model->countByQueries();

		$this->r['data'] = $userResponse;
		$this->response($this->r);
	}

	public function create_post(){
		$requestData = json_decode($this->request->body, true);
		$requestData = $this->_validatePost($requestData);
		$insertData = [
			'name' => $requestData['name'],
			'birth_day' => $requestData['birth_day'],
			'sex' => $requestData['sex'],
			'balance' => $requestData['balance'],
			'created_date' => $requestData['created_date'],
		];
		$this->user_model->create($insertData);

		$this->r['message'] = 'success';
		$this->response($this->r);
	}

	private function _validatePost($requestData = array())
	{
		$httpBadRequestStatusCode = 400;
		if (!isset($requestData['name']) || empty($requestData['name'])) {
			show_error("Name is required", $httpBadRequestStatusCode);
		}
		if (!isset($requestData['birth_day']) || empty($requestData['birth_day'])) {
			show_error("Birthday is required", $httpBadRequestStatusCode);
		}
		if (!$this->_validateDate($requestData['birth_day'])) {
			show_error("Birthday must be in format Y-m-d", $httpBadRequestStatusCode);
		}
		if (!isset($requestData['sex']) || empty($requestData['sex'])) {
			show_error("Gender is required", $httpBadRequestStatusCode);
		}
		if (!in_array($requestData['sex'], [0,1])) {
			show_error("Gender is invalid", $httpBadRequestStatusCode);
		}
		if (!isset($requestData['balance']) || empty($requestData['balance'])) {
			show_error("Balance is required", $httpBadRequestStatusCode);
		}
		if (!is_numeric($requestData['balance'])) {
			show_error("Balance must be numeric type", $httpBadRequestStatusCode);
		}
		if (!isset($requestData['created_date']) || empty($requestData['created_date'])) {
			show_error("Create date is required", $httpBadRequestStatusCode);
		}
		if (!$this->_validateDate($requestData['created_date'])) {
			show_error("Create date must be in format Y-m-d", $httpBadRequestStatusCode);
		}

		return  [
			'name' => $requestData['name'],
			'birth_day' => $requestData['birth_day'],
			'sex' => $requestData['sex'],
			'balance' => $requestData['balance'],
			'created_date' => $requestData['created_date'],
		];
	}

	private function _validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) === $date;
	}
}

?>
