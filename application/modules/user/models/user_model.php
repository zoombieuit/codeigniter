<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/modules/user/models/base_model.php';

class user_model extends base_model {

	private $collection = 'user';

	public function __construct() {
		parent::__construct();
	}

	public function getList($params = array()) {
		if (isset($params['limit']) && !empty($params['limit'])) {
			$this->mongo_db->limit($params['limit']);
		}
		if (isset($params['skip']) && !empty($params['skip'])) {
			$this->mongo_db->offset($params['skip']);
		}
		return $this->mongo_db->get($this->collection);
	}

	public function create($data) {
		$insertedData = [
			'name' => $data['name'],
			'birth_day' => $data['birth_day'],
			'sex' => $data['sex'],
			'balance' => $data['balance'],
			'created_date' => strtotime($data['created_date']),
		];

		$this->mongo_db->insert($this->collection, $insertedData);
	}

	public function countAll() {
		return $this->mongo_db->count($this->collection);
	}

	public function countByQueries($params = array()) {
		return $this->mongo_db->where($params)->count($this->collection);
	}
}

?>
