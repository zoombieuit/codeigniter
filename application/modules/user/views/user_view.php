<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.0/jquery.js"></script>

    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
<div style="align:center;">
    <table  id="user_list"  class="table table-striped display dataTable" style="width:100%" role="grid" cellspacing="0">
        <thead>
        <tr>
            <th>Name</th>
            <th>Birthday</th>
            <th>Gender</th>
            <th>Balance</th>
            <th>Date Create</th>
        </tr>
        </thead>
    </table>
</div>

<script>
    $(document).ready( function () {
        getListUser();
    } );

    function getListUser(){
        $.fn.dataTable.ext.errMode="throw";
        var baseUrl = "http://codeigniter.local/";
        var table = $('#user_list').DataTable({
            //không thay đổi config dưới
            "ordering": true,"autoWidth": true,"scrollCollapse": true,"scrollY": true,"processing": true,"destroy": true,"processing": true,"async": true,"serverSide": true,
            // thay đổi đường dẫn tới API
            "ajax": {"url": baseUrl+'user/api',"dataType": "json","method": "POST"},
            "order": [0, 'desc'],
            "dom": 'Bliprtip',
            "buttons": [],
            "columns": [
                {'data':'name'},
                {'data':'birth_day'},
                {
                    'data':'sex',
                    'render': function(data,type,row) {
                        return data == 1 ? 'Male' : "Female";
                    }
                },
                {'data':'balance'},
                {'data':'created_date'},
            ],
            "fixedHeader": true,
            "drawCallback": function ($e) {
                addSearchDom(this);
            },
        });
    }

    function addSearchDom(table)
    {
        console.log($(table));ec

        $('#user_list thead tr').clone().appendTo('#user_list thead');

        $('#user_list thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            // $( 'input', this ).on( 'keyup change', function () {
            //     if ( table.column(i).search() !== this.value ) {
            //         table
            //             .column(i)
            //             .search( this.value )
            //             .draw();
            //     }
            // });
        });
    }
</script>
</body>
