<h1>{title}</h1>
<p><?php var_dump($param);?></p>

<!--

Sử dụng AJAX,JS để PosT Create Form.
Sử dụng Lib Datatables.net để đưa danh sách ra hiển thị.
Sử dụng Bootraps thuần để tạo layout.
Sử dụng Jquery 3.2

Ví dụ function Datatables
HTML TABLES
<table  id="donhang_history"  class="table table-striped display dataTable" style="width:100%" role="grid" cellspacing="0">
			<thead>
				<tr>
					<th>Mã đơn hàng</th>
					<th>Ngày mua</th>
					<th>Tên sản phẩm</th>
					<th>Số lượng</th>
					<th>Đơn giá</th>
					<th>Thành tiền</th>
					<th>Trạng thái</th>
				</tr>
			</thead>
		</table>
JS TABLES
function donhang_history(){
		$.fn.dataTable.ext.errMode="throw";
		
		var table = $('#donhang_history').DataTable({
		//không thay đổi config dưới 
		"ordering": true,"autoWidth": true,"scrollCollapse": true,"scrollY": true,"processing": true,"destroy": true,"processing": true,"async": true,"serverSide": true,
		// thay đổi đường dẫn tới API
		"ajax": {"url": BASE_URL+'profile/api/don_hang',"dataType": "json","method": "GET"},
		"order": [0, 'desc'],
		"dom": 'Bliprtip',
		"buttons": [],
		"columns": [ // số cột hiển thị bảng.
			{'data':'id_cart_item'},
			{'data':'date_create'},
			{'data':'product_title',render:function(data,type,row){
				return row.product_title;
			}},
			{'data':'id_cart_item',render:function(data,type,row){
				if(row.quantity !== 'undefined' && row.quantity !== null && row.quantity !== ''){ return row.quantity; }
				else{ return 0; }
			}},
			{'data':'id_cart_item',render:function(data,type,row){
				if(row.price !== 'undefined' && row.price !== null && row.price !== ''){ return row.price; }
				else{ return 0; }
			}},
			{'data':'id_cart_item',render:function(data,type,row){
				if(row.amount !== 'undefined' && row.amount !== null && row.amount !== ''){ return row.amount; }
				else{ return 0; }
			}},
			{'data':'id_cart_item'},
		],
    
	});
}
-->